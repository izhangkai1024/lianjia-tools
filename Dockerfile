FROM node:18.14.0

RUN mkdir workspace
WORKDIR /workspace

COPY . .
RUN npm install

EXPOSE 8080

ENV NODE_ENV=production
ENV PORT=8080

ENTRYPOINT ["npm", "run", "start"]