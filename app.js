const express = require('express');
const app = express();
const port = process.env.PORT || "8088"

app.use(express.static('public', { etag: false }))

app.listen(port, () => console.log('listening on ' + port));